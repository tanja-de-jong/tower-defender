import World from "./items/world.js";
import Tower from "./items/tower.js";
import Player from "./items/player.js";
import HUD from "./items/hud.js";
import GameDescription from "./items/game_description.js";
import Enemy from "./items/enemy.js";
import GameOver from "./items/game_over.js";
import level2 from '../levels/2.json' assert { type: 'json' };

export default class Game {

	constructor(ctx, canvas) {
		this.ctx = ctx;
		this.canvas = canvas;

		this.dt = undefined;
		this.oldTimestamp = 10;
		this.world = null;
		this.player = null;
		this.hud = null
		this.towers = [];
		this.enemies = [];
		this.enemyIntervals = [];
		this.start = false;

		new GameDescription(this.ctx, () => this.start = true).draw();

        document.addEventListener("keydown", this.startGameHandler);
	}

	startGameHandler = (event) => {
		if (event.code == 'Enter') {
			this.startGame();
			document.removeEventListener("keydown", this.startGameHandler);
		}
	}

	startGame() {	
		this.world = new World(this.ctx);
		this.player = new Player(this.ctx, 3, 3, this);
		this.hud = new HUD(this.ctx, 1000, 100);
		this.enemyIntervals.push(setInterval(() => this.spawnEnemy(3*40+20, 7*40+20, 'red'), level2.interval));
		this.enemyIntervals.push(setInterval(() => this.spawnEnemy(7*40+20, 12*40+20, 'blue'), level2.interval));

		window.requestAnimationFrame(this.gameLoop);
	}

	gameLoop = (timestamp) => {
		this.dt = (timestamp - this.oldTimestamp) / 1000;
		this.oldTimestamp = timestamp;

		this.draw(this.dt);

		if (this.hud.getHealth() > 0) {
			window.requestAnimationFrame(this.gameLoop);
		} else {
			new GameOver(this.ctx, this.startGame).draw();
		}
	}

	// function reset() {
	// 	canvas = undefined;
	// 	ctx = undefined;
	// 	dt = undefined;
	// 	oldTimestamp = undefined;
	// 	world = null;
	// 	player = null;
	// 	hud = null
	// 	towers.forEach(tower => tower.resetBullets());
	// 	towers = [];
	// 	enemies = [];
	// 	enemyIntervals.forEach(itv => clearInterval(itv));
	// 	enemyIntervals = [];

	// 	init();
	// }

	draw(dt) {
		// clear screen
		this.ctx.clearRect(0, 0, canvas.width, canvas.height);

		// draw items
		this.world.draw();
		this.player.draw();
		this.hud.draw();
		this.towers.forEach(tower => tower.draw(dt));
		this.enemies.forEach(enemy => enemy.draw(dt));
	}

	buildTower(x, y) {
		if (this.world.isGrass(x, y) && this.fieldIsAvailable(x, y)) {
			if (this.hud.add(-100)) this.towers.push(new Tower(this.ctx, x, y, this));
		}
	}

	killEnemy() {
		this.hud.add(10);
	}

	getEnemies() {
		return this.enemies;
	}
	
	removeEnemy(enemy) {
		this.enemies = this.enemies.filter(e => e != enemy);
	}

	fieldIsAvailable(x, y) {
		return !this.towers.some(tower => {
			return tower.atPosition(x, y);
		})
	}

	getHit() {
		this.hud.decreaseHealth();
	}

	spawnEnemy(x, y, type) {
		this.enemies.push(new Enemy(this.ctx, x, y, type, this.world, this));
	}

}