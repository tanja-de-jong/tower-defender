export default class Enemy {

	constructor(context, x, y, type, world, game) {
		this.ctx = context;
		this.x = x;
		this.y = y;
		this.type = type;
		this.world = world;
		this.dir = 'right';
		this.active = true;
		this.game = game;
		this.health = 15;
	}

	speed = 50;

	draw(dt) {
		if (this.active) {
			// get direction
			this.dir = this.world.getDirection(this.dir, this.x, this.y);

			if (this.dir == 'finished') this.hitBase();

			if (this.dir == 'right') {
				this.x += this.speed * dt;
			} else if (this.dir == 'up') {
				this.y -= this.speed * dt;
			} else if (this.dir == 'down') {
				this.y += this.speed * dt;
			}


			if (this.type == 'red') {
				this.ctx.fillStyle = 'rgba(255, 0, 0,' + this.health / 15 + ')';
			} else if (this.type == 'blue') {
				this.ctx.fillStyle = 'rgba(0, 0, 255,' + this.health / 15 + ')';
			}

			this.ctx.beginPath();
			this.ctx.arc(this.x, this.y, 10, 0, Math.PI*2, false);
			this.ctx.closePath();
			this.ctx.fill();
		}
	}

	hitBase() {
		this.active = false;
		this.game.getHit();
	}

	getPos() {
		return {x: this.x, y: this.y};
	}

	reduceHealth(strength) {
		this.health -= strength;
		if (this.health <= 0) {
			this.game.removeEnemy(this);
			return true;
		}
	}
}
