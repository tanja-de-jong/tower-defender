import level1 from '../levels/1.json' assert { type: 'json' };
import level2 from '../levels/2.json' assert { type: 'json' };

export default class World {

	constructor(context) {
		this.ctx = context;
		this.grid = level2.level;
		console.log("World created");
	}

	draw() {
		for (let y = 0; y < this.grid.length; y++) {
            for (let x = 0; x < this.grid[y].length; x++) {
				this.drawTile(x, y, this.grid[y][x]);
            }
        }
	}

	drawTile(x, y, val) {
		switch (val) {
			case 0: // empty field
				this.ctx.fillStyle = '#A8D899';
				break;
			case 1: // road
				this.ctx.fillStyle = '#D8C399';
				break;
			case 20: // spawn point 0
				this.ctx.fillStyle = '#FF0000';
				break;
			case 21: // spawn point 1
				this.ctx.fillStyle = '#0000FF'
				break;
			case 30: // base
				this.ctx.fillStyle = '#00FF00';
				break;
			default:
				break;
		}
		this.ctx.fillRect(x * 40 + 1, y * 40 + 1, 38, 38);
	}

	isGrass(x, y) {
		return this.grid[y][x] == 0;
	}

	isPath(dir, wx, wy) {
		if (dir == 'right') {
			return this.grid[wy][wx+1] == 1 || this.grid[wy][wx+1] == 30;
		} else if (dir == 'up') {
			return this.grid[wy-1][wx] == 1 || this.grid[wy][wx+1] == 30;
		} else if (dir == 'down') {
			return this.grid[wy+1][wx] == 1 || this.grid[wy][wx+1] == 30;
		}
	}

	hasReachedCenter(currDir, x, y) {
		if (currDir == 'down') {
			let relPos = y % 40;
			return relPos >= 20 && relPos <= 39;
		} else if (currDir == 'up') {
			let relPos = y % 40;
			return relPos >= 0 && relPos <= 19;
		} else if (currDir == 'right') {
			let relPos = x % 40;
			return relPos >= 20 && relPos <= 39;
		} else if (currDir == 'left') {
			let relPos = x % 40;
			return relPos >= 0 && relPos <= 19;
		}
	}

	getDirection(currDir, x, y) {
		let wx = Math.floor((x) / 40);
		let wy = Math.floor(y / 40);

		if (!this.hasReachedCenter(currDir, x, y)) return currDir;

		if (this.isPath('right', wx, wy)) {
			return 'right';
		} else if (this.isPath('up', wx, wy) && currDir != 'down') {
			return 'up';
		} else if (this.isPath('down', wx, wy) && currDir != 'up') {
			return 'down';
		} else {
			return 'finished';
		}
	}

}
