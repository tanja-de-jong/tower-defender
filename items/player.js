import { HEIGHT, WIDTH } from "../constants.js";
import Game from "../game.js";


export default class Player {

	constructor(context, x, y, game) {
		this.ctx = context;
        this.x = x;
        this.y = y;

        document.addEventListener("keydown", event => {
            switch (event.code) {
                case 'ArrowUp':
                    if (this.y > 1) this.y = this.y - 1;
                    break;
                case 'ArrowDown':
                    if (this.y < HEIGHT - 1) this.y = this.y + 1;
                    break;
                case 'ArrowLeft':
                    if (this.x > 0) this.x = this.x - 1;
                    break;
                case 'ArrowRight':
                    if (this.x < WIDTH - 1) this.x = this.x + 1;
                    break;
                case 'Space':
                    game.buildTower(this.x, this.y);
                    break;
                default:
                    break;
            }
        });
	}

	draw() {
        this.ctx.strokeStyle = '#FF0000';
		this.ctx.lineWidth = 2;
		this.ctx.strokeRect(this.x * 40, this.y * 40, 40, 40);
	}

}
