import Bullet from "./bullet.js";

export default class Tower {
	bullets = [];
	bulletIntervals = []

	constructor(context, col, row, game) {

		this.ctx = context;
        this.col = col;
        this.row = row;
		this.game = game;
		this.strength = 5;

		this.bulletIntervals.push(setInterval(() => this.addBullet(), 750));
	}

	draw(dt) {
		const a = 2*Math.PI / 6;
		const r = 18;
		this.ctx.beginPath();
		for (let i = 0; i < 6; i++) {
			this.ctx.lineTo((this.col*40 + 20) + r * Math.cos(a * i), (this.row*40 + 20) + r * Math.sin(a * i));
		}
		this.ctx.closePath();
		this.ctx.fillStyle = '#808080';
		this.ctx.fill();

		this.bullets.forEach(bullet => {
			bullet.drawB(dt);
		});
	}

    atPosition(col, row) {
        return this.col == col && this.row == row;
    }

	addBullet() {
		this.bullets.push(new Bullet(this.ctx, this.col*40+20, this.row*40+20, this.game, this));
	}

	hit(bullet) {
		this.bullets = this.bullets.filter(b => b != bullet)
		let killed = bullet.getTarget().reduceHealth(this.strength);
		if (killed) this.game.killEnemy();
	}

	resetBullets() {
		this.bulletIntervals.forEach(itv => clearInterval(itv));
		this.bullets = [];
	}
}
