import { HEIGHT, WIDTH } from "../constants.js";

export default class GameOver {

    constructor(context, startGame) {
      this.ctx = context;
      this.startGame = startGame;
      
      // function startGameHandler(event) {
      //   console.log(event);
      //   if (event.code == 'Enter') {
            
      //       startGame();
      //       document.removeEventListener("keydown", startGameHandler);
      //   } 
      //   // else if (event.code == 'Escape') {
      //   //     document.removeEventListener("keydown", startGameHandler);
      //   // }
      // } 

      // document.addEventListener("keydown", startGameHandler);
    }

    draw() {
		  this.ctx.fillStyle = '#000000';
      this.ctx.fillRect(0, 0, WIDTH * 40, HEIGHT * 40);
     
      this.ctx.font = "40px Text";
      this.ctx.textAlign = "center";
      this.ctx.textBaseline = "center";
      this.ctx.fillStyle = "#FF0000";

      this.ctx.fillText("GAME OVER", WIDTH * 40 / 2, HEIGHT * 40 / 2);

      // this.ctx.font = "20px Text";
      // // this.ctx.textAlign = "start";
      // this.ctx.textBaseline = "bottom";
      // this.ctx.fillStyle = "#FFFFFF";
      // // this.ctx.fillText("[ Press Esc to quit ]", 20, HEIGHT * 40 - 20);

      // this.ctx.textAlign = "center";
      // this.ctx.fillText("[ Press Enter to play again ]", WIDTH * 40 / 2, HEIGHT * 40 - 20);
    
	}
}
