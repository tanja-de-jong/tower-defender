export default class Bullet {

	constructor(context, x, y, game, tower) {
		this.ctx = context;
		this.x = x;
		this.y = y;
		this.game = game;
		this.tower = tower;
		this.rotation = undefined;
		this.target = this.selectEnemy(game.getEnemies());
	}

	speed = 100;

	getTarget() {
		return this.target;
	}

	drawB(dt) {
		if (this.target != undefined) {
			let targetPos = this.target.getPos();

			if (!this.game.getEnemies().includes(this.target)) {
				// Enemy no longer exists
				this.ctx.fillStyle = '#606060';
				this.ctx.beginPath()
				this.ctx.arc(this.x, this.y, 3, 0, Math.PI*2);
				this.ctx.closePath();
				this.ctx.fill();

				this.x += Math.cos(this.rotation) * this.speed * dt;
				this.y -= Math.sin(this.rotation) * this.speed * dt;
			} else if (this.withinRange(targetPos)) {
				this.tower.hit(this);
			} else {
				this.ctx.fillStyle = '#606060';
				this.ctx.beginPath()
				this.ctx.arc(this.x, this.y, 3, 0, Math.PI*2);
				this.ctx.closePath();
				this.ctx.fill();

				this.rotation = Math.atan2((this.y - targetPos.y), (targetPos.x - this.x));

				this.x += Math.cos(this.rotation) * this.speed * dt;
				this.y -= Math.sin(this.rotation) * this.speed * dt;
			}
		}
	}
	
	withinRange(targetPos) {
		return Math.abs(targetPos.x - this.x) < 10 && Math.abs(targetPos.y - this.y) < 10;
	}

	selectEnemy(enemies) {
		return enemies.length > 0 ? enemies.reduce((a, b) => {
			let posA = a.getPos();
			let posB = b.getPos();
			return this.getDistance(this.x, this.y, posA.x, posA.y) < this.getDistance(this.x, this.y, posB.x, posB.y) ? a : b;
		}) : undefined;
	}

	getDistance(x1, y1, x2, y2) {
		return Math.sqrt(Math.pow(Math.abs(x1 - x2), 2) + Math.pow(Math.abs(y1 - y2), 2))
	}
}