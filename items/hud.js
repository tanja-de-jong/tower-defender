import { WIDTH } from "../constants.js";

export default class HUD {

    constructor(context, amount, health) {
		this.ctx = context;
        this.amount = amount;
        this.health = health;
	}

    draw() {
		this.ctx.fillStyle = '#000000';
        this.ctx.fillRect(0, 0, WIDTH * 40, 39);
        this.ctx.font = "20px Text";
        this.ctx.textAlign = "right";
        this.ctx.textBaseline = "top";
        this.ctx.fillStyle = "#ffffff";

        this.ctx.fillText("Health: " + this.health + "     Coins: " + this.amount, WIDTH * 40 - 10, 10);
	}

    add(amount) {
        if (this.amount < -amount) {
            return false;
        } else {
            this.amount += amount;
            return true;
        }
    }

    decreaseHealth() {
        this.health = this.health >= 10 ? this.health - 10 : 0;
        return this.health;
    }

    getHealth() {
        return this.health;
    }

}
