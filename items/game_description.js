import { HEIGHT, WIDTH } from "../constants.js";

export default class GameDescription {

    constructor(context, start) {
		this.ctx = context;

        function startGameHandler(event) {
            console.log(event);
            if (event.code == 'Enter') {
                start();
                document.removeEventListener("keydown", startGameHandler);
            }
        }

        document.addEventListener("keydown", startGameHandler);
	}

    draw() {
		this.ctx.fillStyle = '#000000';
        this.ctx.fillRect(0, 0, WIDTH * 40, HEIGHT * 40);
        this.ctx.font = "40px Text";
        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "top";
        this.ctx.fillStyle = "#ffffff";

        this.ctx.fillText("TOWER DEFENDER", WIDTH * 40 / 2, 25);

        this.ctx.font = "20px Text";
        this.ctx.textAlign = "left";
        this.ctx.textBaseline = "center";
        this.ctx.fillStyle = "#ffffff";

        this.ctx.fillText("- Move around using arrow keys.", 20, 130);
        this.ctx.fillText("- Build towers for 100 coins on strategic locations to defeat your enemies.", 20, 160);
        this.ctx.fillText("- You can build a tower on your location by pressing Space.", 20, 190);
        this.ctx.fillText("- Don't let your enemies mix, because then they will become a lot stronger!", 20, 220);

        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "bottom";
        this.ctx.fillText("[ Press Enter to start the game ]", WIDTH * 40 / 2, HEIGHT * 40 - 40);
	}
}
